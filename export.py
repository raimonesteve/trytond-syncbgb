# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.

import logging

from trytond.model import ModelView, ModelSQL, ModelSingleton, fields
from trytond.pyson import Eval,Bool,Or,Not
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateView, StateTransition, Button, StateAction

try:
    import psycopg2
except ImportError:
    psycopg2 = None

__all__ = ['FranchisePriceList', 'ExportPricesStart', 'ExportPrices',
    'ManualExportPrices', 'ManualExportPricesStart']
__metaclass__ = PoolMeta


class FranchisePriceList:
    __name__ = 'sale.franchise.price_list'

    @classmethod
    def export_cron(cls):
        pool = Pool()
        Wizard = pool.get('syncbgb.export_prices', type='wizard')
        session_id, _, _ = Wizard.create()
        wizard = Wizard(session_id)
        wizard.getSalesAndExport()  # it raise an exeption if cant use DB
        return True


class ExportPricesStart(ModelView):
    'Export Prices Start'
    __name__ = 'syncbgb.export_prices.start'

    date = fields.Date('Date', required=True)

    @staticmethod
    def default_date():
        Date = Pool().get('ir.date')
        return Date.today()


class ExportPrices(Wizard):
    'Export Prices'
    __name__ = 'syncbgb.export_prices'

    start = StateView('syncbgb.export_prices.start',
    'syncbgb.export_prices_start_view_form', [
        Button('Cancel', 'end', 'tryton-cancel'),
        Button('Export', 'getSales', 'tryton-ok', default=True),
        ])

    getSales = StateTransition()

    @classmethod
    def __setup__(cls):
        super(ExportPrices, cls).__setup__()
        cls._error_messages.update({
            'unknown_uom': 'The uom "%s" is not in syncbgb configuration.',
            'unknown_tax': 'The tax "%s" is not in syncbgb configuration.',
            'no_tax': 'The product "%s" need to have a tax.'
        })

    def export(self, items, date):
        pool = Pool()
        Config = pool.get('syncbgb.config')
        Product = pool.get('product.product')
        User = pool.get('res.user')
        Tax = pool.get('account.tax')

        # Logging
        logger = logging.getLogger('syncbgb')

        logger.info('Starting syncronitzation of prices')

        # Before start, assert the connection
        Wizard = pool.get('syncbgb.test_connection', type='wizard')
        session_id, _, _ = Wizard.create()
        wizard = Wizard(session_id)
        wizard.transition_test()  # it raise an exeption if cant use DB

        # Get config (singleton)
        config = Config(1)

        conn = psycopg2.connect(host=config.host, port=config.port,
            database=config.database, user=config.user,
            password=config.password)
        cur = conn.cursor()

        logger.info('Successfully connected to DB')

        # pricelist don't have serial pk, so we have to calc it
        cur.execute("SELECT MAX(recid) FROM pricelist")
        recid = cur.fetchone()[0] or 1

        for item in items:
            # An item have a product, a franchise and a pvp
            franchise = item['franchise']
            with Transaction().set_context(
                    company=(User(Transaction().user).company.id)):
                product = Product(item['product'])
                pvp = item['pvp']

                # Product needs a PLU, numeric and ">= 0 && <= 9999"
                plu = product.template.plu
                try:
                    if int(plu) < 0 or int(plu) > 9999:
                        continue  # under or overranged
                except Exception:
                    continue  # not numeric PLU

                # Mappings
                if product.template.default_uom == config.uom_un:
                    txtUOM = "Un."
                elif product.template.default_uom == config.uom_kg:
                    txtUOM = "Kg"
                else:
                    self.raise_user_error('unknown_uom',
                        (product.template.default_uom.name,))

                # L'iva s'agafa el primer, ja que es pot ordenar
                taxes = product.customer_taxes_used
                if taxes:
                    tax = taxes[0]

                    if not tax.rate:  # Is a rule tax?
                        # Use tax rule to get real franchise (PVP) tax
                        tax = Tax(config.tax_rule.apply(tax, {})[0])

                    if config.tax_sre.rate == tax.rate:
                        txtIVA = "00"
                    elif config.tax_red.rate == tax.rate:
                        txtIVA = "01"
                    elif config.tax_gen.rate == tax.rate:
                        txtIVA = "02"
                    else:
                        self.raise_user_error('unknown_tax', (tax.name,))
                else:
                    self.raise_user_error('no_tax', (product.template.name,))

                # Si hi ha barcode, s'agafa el primer (es pot ordenar)
                barcode = product.code_ean13

                # Entrades a pricelist desde Tryton es departament 0001
                dep = "0001"

                recid += 1

                cur.execute("INSERT INTO pricelist \
                    (recid,VCTINVENTLOCATIONIDSHOP,VCTPLU,PRICE,SALESUNIT,\
                    NAME,DEL_DATEEXPECTED,DATESHOP,DATAAREAID,CodBarras,iva,\
                    tipo,DEP,recidax,fam,datetimesent,confirmed,vctfactorkg) \
                    VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,\
                    %s,%s,%s)",
                    (recid, franchise.code, plu, pvp, txtUOM,
                    product.template.name, date, date, 'all', barcode,
                    txtIVA, '', dep, recid, dep, 0, 0, 0))

                logger.info('Inserted %s to store %s with price %s' %
                    (product.template.name, franchise.code, pvp))

        conn.commit()

        cur.close()
        conn.close()

        logger.info('All exported. Finishing...')

        return True

    def getSalesAndExport(self, date=None):
        pool = Pool()
        SaleLine = pool.get('sale.line')
        Date = pool.get('ir.date')

        if not date:
            date = Date.today()

        items = []

        lines = SaleLine.search([
            ('sale.sale_date', '=', date),
            ('sale.state', '!=', 'cancel')
        ])
        for line in lines:
            if line.public_price is None or line.sale.franchise is None:
                continue  # No te sentit exportar-ho, peta

            item = {}
            item['franchise'] = line.sale.franchise
            item['product'] = line.product
            item['pvp'] = line.public_price
            items.append(item)

        self.export(items, date)

        return True

    def transition_getSales(self):
        self.getSalesAndExport(self.start.date)
        return "end"


class ManualExportPricesStart(ModelView):
    'Manual Export Prices Start'
    __name__ = 'syncbgb.manual_export_prices.start'


class ManualExportPrices(Wizard):
    'Manual Export Prices'
    __name__ = 'syncbgb.manual_export_prices'

    start = StateView('syncbgb.manual_export_prices.start',
    'syncbgb.manual_export_prices_start_view_form', [
        Button('Cancel', 'end', 'tryton-cancel'),
        Button('Export', 'getPrices', 'tryton-ok', default=True),
        ])

    getPrices = StateTransition()

    def transition_getPrices(self):
        pool = Pool()

        Price = pool.get('sale.franchise.price_list')
        Date = pool.get('ir.date')

        items = []
        date = Date.today()

        for price in Price.browse(Transaction().context['active_ids']):

            franchises = price.franchises
            if not len(franchises):
                franchises = price.product.franchises

            for franchise in franchises:
                item = {}
                item['franchise'] = franchise
                item['product'] = price.product
                item['pvp'] = price.public_price
                items.append(item)

        # Use the export_prices export method
        Wizard = pool.get('syncbgb.export_prices', type='wizard')
        session_id, _, _ = Wizard.create()
        wizard = Wizard(session_id)
        wizard.export(items, date)

        return "end"
