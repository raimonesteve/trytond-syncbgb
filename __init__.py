# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from .config import *
from .ticket import *
from .export import *

def register():
    Pool.register(
        Config,
        TestConnectionSucceed,
        Franchise,
        Party,
        InvoiceLine,
        Aeat340Record,
        Ticket,
        TicketLine,
        ImportTicketsStart,
        ReimportTicketsStart,
        ExportPricesStart,
        ManualExportPricesStart,
        CreateInvoicesStart,
        ReinvoiceStart,
        FranchisePriceList,
        module='syncbgb', type_='model')
    Pool.register(
        TestConnection,
        ImportTickets,
        ReimportTickets,
        ExportPrices,
        ManualExportPrices,
        CreateInvoices,
        Reinvoice,
        module='syncbgb', type_='wizard')
