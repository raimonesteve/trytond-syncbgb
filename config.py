# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.

from trytond.model import ModelView, ModelSQL, ModelSingleton, fields
from trytond.pyson import Eval, Bool, Or, Not
from trytond.config import config
from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateView, StateTransition, Button

try:
    import psycopg2
except ImportError:
    psycopg2 = None

__all__ = ['Config', 'TestConnection', 'TestConnectionSucceed']


class Config(ModelSingleton, ModelSQL, ModelView):
    'Config'
    __name__ = 'syncbgb.config'

    # SymDB Connection
    host = fields.Function(
        fields.Char('SymDB Host', required=True),
        'get_str_config_parameter')
    port = fields.Function(
        fields.Integer('SymDB Port', required=True),
        'get_int_config_parameter')
    user = fields.Function(
        fields.Char('SymDB User', required=True),
        'get_str_config_parameter')
    password = fields.Function(
        fields.Char('SymDB Password', required=True),
        'get_str_config_parameter')
    database = fields.Function(
        fields.Char('SymDB Database', required=True),
        'get_str_config_parameter')

    # Mapping helpers
    no_plu = fields.Many2One('product.product', 'Unknown PLU', required=True)
    invoice_plu = fields.Many2One('product.product', 'Invoice PLU',
        required=True)

    uom_un = fields.Many2One('product.uom', 'UOM Un.', required=True)
    uom_kg = fields.Many2One('product.uom', 'UOM Kg.', required=True)

    payment_cash = fields.Many2One('account.payment.type',
        'Cash Payment Type', required=True)
    payment_card = fields.Many2One('account.payment.type',
        'Card Payment Type', required=True)
    payment_credit = fields.Many2One('account.payment.type',
        'Credit Payment Type', required=True)

    tax_sre = fields.Many2One('account.tax.template', 'TAX SRE', required=True)
    tax_red = fields.Many2One('account.tax.template', 'TAX RED', required=True)
    tax_gen = fields.Many2One('account.tax.template', 'TAX GEN', required=True)
    tax_rule = fields.Many2One('account.tax.rule', 'TAX Rule', required=True)

    group_plu = fields.Many2One('product.product', 'Gropued lines PLU',
        required=True)

    @classmethod
    def __setup__(cls):
        super(Config, cls).__setup__()

        cls._buttons.update({
            'test': {
                'invisible': Or(
                    Not(Bool(Eval('host'))),
                    Not(Bool(Eval('user'))),
                    Not(Bool(Eval('password'))),
                    Not(Bool(Eval('database')))
                )
            }
        })

    @classmethod
    @ModelView.button
    @ModelView.button_action('syncbgb.wizard_test_connection')
    def test(cls, configs):
        pass  # wizard!

    def get_str_config_parameter(self, name=None):
        if name:
            return config.get('syncbgb', name)

    def get_int_config_parameter(self, name=None):
        if name:
            return config.getint('syncbgb', name)


class TestConnectionSucceed(ModelView):
    'Test Connection'
    __name__ = 'syncbgb.test_connection.succeed'


class TestConnection(Wizard):
    'Test Connection'
    __name__ = 'syncbgb.test_connection'

    start_state = 'test'

    test = StateTransition()

    succeed = StateView('syncbgb.test_connection.succeed',
        'syncbgb.wizard_test_connection_succeed_view_form', [
            Button('Ok', 'end', 'tryton-ok', default=True),
            ])

    @classmethod
    def __setup__(cls):
        super(TestConnection, cls).__setup__()
        cls._error_messages.update({
            'no_driver': 'Module psycopg2 not installed',
            'cant_connect': ('Can\'t connect to the provided database.'
                ' Please, check the configuration parameters.'),
            'cant_insert': ('Can\'t insert in the test table.'
                ' Please, check the user permissions'),
            'cant_delete': ('Can\'t delete in the test table.'
                ' Please, check the user permissions')
        })

    def transition_test(self):
        # Get config (singleton)
        Config = Pool().get('syncbgb.config')
        config = Config(1)

        if not psycopg2:
            self.raise_user_error('no_driver')

        # Test connection
        try:
            conn = psycopg2.connect(host=config.host, port=config.port,
                database=config.database, user=config.user,
                password=config.password)
            cur = conn.cursor()
        except:
            self.raise_user_error('cant_connect')

        # Test insert on test table (id,nombre,edad)
        try:
            cur.execute("INSERT INTO test (id,nombre,edad) VALUES (%s,%s,%s)",
                (1123581321, "test", 42))
            cur.execute("SELECT * FROM test WHERE id = %s", (1123581321,))
            line = cur.fetchone()
            if line[2] != 42:
                raise
            conn.commit()
        except:
            self.raise_user_error('cant_insert')

        # Test delete
        try:
            cur.execute("DELETE FROM test WHERE id = %s", (1123581321,))
            conn.commit()
            cur.close()
            conn.close()
        except:
            self.raise_user_error('cant_delete')

        # All ok, change state
        return 'succeed'
