# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from collections import defaultdict
from decimal import Decimal
import datetime
import logging
import pytz

from trytond.model import ModelView, ModelSQL, Unique, fields
from trytond.pyson import Bool, Eval
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateView, Button, StateAction
from sql import Null
from sql.aggregate import Count


try:
    import psycopg2
except ImportError:
    psycopg2 = None

__all__ = ['Franchise', 'Party', 'InvoiceLine', 'Aeat340Record',
    'Ticket', 'TicketLine',
    'ImportTicketsStart', 'ImportTickets', 'ReimportTicketsStart',
    'ReimportTickets', 'CreateInvoicesStart', 'CreateInvoices',
    'ReinvoiceStart', 'Reinvoice']
__metaclass__ = PoolMeta


class Franchise:
    __name__ = 'sale.franchise'
    ticket_party = fields.Many2One('party.party', 'Ticket Party',
        help='The party that will be used for invoice tickets without party')
    tickets = fields.One2Many('syncbgb.ticket', 'franchise', 'Tickets')


class Party:
    __name__ = 'party.party'
    tickets = fields.One2Many('syncbgb.ticket', 'party', 'Tickets')


class InvoiceLine:
    __name__ = 'account.invoice.line'

    ticket_lines = fields.One2Many('syncbgb.ticket.line', 'invoice_line',
        'Ticket Lines')

    @classmethod
    def _get_origin(cls):
        models = super(InvoiceLine, cls)._get_origin()
        models.append('syncbgb.ticket.line')
        return models


class Aeat340Record:
    __name__ = 'aeat.340.record'

    def get_ticket_count(self, name):
        pool = Pool()
        Ticket = pool.get('syncbgb.ticket')

        if self.operation_key == 'B':
            if self.invoice_lines:
                n_tickets = Ticket.search([
                            ('lines.invoice_line', 'in',
                                [il.id for il in self.invoice_lines])
                            ], count=True)
                return n_tickets
        return super(Aeat340Record, self).get_ticket_count(name)

    def get_first_last_invoice_number(self):
        pool = Pool()
        Ticket = pool.get('syncbgb.ticket')
        if self.operation_key == 'B' and self.invoice_lines:
            tickets = Ticket.search([
                        ('lines.invoice_line', 'in',
                            [il.id for il in self.invoice_lines])
                        ],
                    order=[('numticket', 'ASC')])
            if tickets:
                return (str(tickets[0].numticket), str(tickets[-1].numticket))
        return super(Aeat340Record, self).get_first_last_invoice_number()

STATES = {
    'readonly': Bool(Eval('invoices')),
}


class Ticket(ModelSQL, ModelView):
    'Ticket'
    __name__ = 'syncbgb.ticket'

    ticket_date = fields.DateTime('Date', required=True, readonly=True,
        select=True)
    idticket = fields.BigInteger('Ticket ID', required=True, readonly=True,
        select=True)
    numticket = fields.Integer('Ticket Number', required=True, readonly=True,
        select=True)
    special_invoice = fields.Boolean('Special Invoice', select=True,
        states=STATES)

    franchise = fields.Many2One('sale.franchise', 'Franchise', required=True,
        readonly=True, select=True)
    party = fields.Many2One('party.party', 'Party', states=STATES, select=True)

    scale = fields.Integer('Scale', required=True, readonly=True)
    seller = fields.Integer('Seller', required=True, readonly=True)
    amount = fields.Float('Amount', digits=(16, Eval('currency_digits', 2)),
        required=True, depends=['currency_digits'], readonly=True)
    net_sre = fields.Float('SRE Net', digits=(16, Eval('currency_digits', 2)),
        depends=['currency_digits'], readonly=True)
    tax_sre = fields.Float('SRE TAX', digits=(16, Eval('currency_digits', 2)),
        depends=['currency_digits'], readonly=True)
    net_red = fields.Float('RED Net', digits=(16, Eval('currency_digits', 2)),
        depends=['currency_digits'], readonly=True)
    tax_red = fields.Float('RED TAX', digits=(16, Eval('currency_digits', 2)),
        depends=['currency_digits'], readonly=True)
    net_gen = fields.Float('GEN Net', digits=(16, Eval('currency_digits', 2)),
        depends=['currency_digits'], readonly=True)
    tax_gen = fields.Float('GEN TAX', digits=(16, Eval('currency_digits', 2)),
        depends=['currency_digits'], readonly=True)
    net_notax = fields.Float('Net without TAX',
        digits=(16, Eval('currency_digits', 2)), depends=['currency_digits'],
        readonly=True)
    payment_type = fields.Many2One('account.payment.type', 'Payment Type',
        required=True, readonly=True)
    percentdiscount = fields.Float('% Discount',
        digits=(16, Eval('currency_digits', 2)), depends=['currency_digits'],
        readonly=True)
    discount = fields.Float('Discount',
        digits=(16, Eval('currency_digits', 2)),
        depends=['currency_digits'], readonly=True)
    currency_digits = fields.Function(fields.Integer('Unit Digits'),
        'on_change_with_currency_digits')
    lines = fields.One2Many('syncbgb.ticket.line', 'ticket', 'Lines',
        readonly=True)
    to_invoice = fields.Function(fields.Boolean('To Invoice'),
        'get_to_invoice', searcher='search_to_invoice')
    invoices = fields.Function(fields.One2Many('account.invoice', None,
            'Invoices'),
        'get_invoices', searcher='search_invoices')
    franchise_with_company = fields.Function(
        fields.Boolean('Franchise with Company'),
        'get_franchise_with_company')

    @classmethod
    def __setup__(cls):
        super(Ticket, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints += [
            ('franchise_idticket_uniq',
                Unique(t, t.franchise, t.idticket, t.ticket_date),
                'Ticket ID - Franchise pair must be unique for a given date.')
        ]
        cls._error_messages.update({
                'missing_party': ('Ticket "%s" misses a party. Please '
                    'define on on the ticket or on the franchise.'),
                })
        cls._buttons.update({
                'invoice': {
                    'invisible': (Bool(Eval('invoices'))
                        | ~Eval('franchise_with_company')),
                    'icon': 'tryton-go-next',
                    },
                })

    @fields.depends('franchise')
    def on_change_with_currency_digits(self, name=None):
        if self.franchise and self.franchise.company:
            return self.franchise.company.currency.digits
        return 2

    def get_to_invoice(self, name):
        return (self.franchise.company != None
            and any(l.to_invoice for l in self.lines))

    @classmethod
    def search_to_invoice(cls, name, clause):
        pool = Pool()
        Franchise = pool.get('sale.franchise')
        TicketLine = pool.get('syncbgb.ticket.line')

        line = TicketLine.__table__()
        ticket = cls.__table__()
        franchise = Franchise.__table__()

        Operator = fields.SQL_OPERATORS[clause[1]]
        query = ticket.join(franchise,
            condition=ticket.franchise == franchise.id
            ).join(line,
                condition=line.ticket == ticket.id
                ).select(ticket.id,
                    where=franchise.company != None,
                    group_by=ticket.id,
                    having=Operator(Count(line.invoice_line) == 0,
                        clause[2]))
        return [('id', 'in', query)]

    def get_invoices(self, name):
        invoices = set()
        for line in self.lines:
            if line.invoice_line and line.invoice_line.invoice:
                invoices.add(line.invoice_line.invoice.id)
        return list(invoices)

    @classmethod
    def search_invoices(cls, name, clause):
        return [('lines.invoice_line.invoice',) + tuple(clause[1:])]

    def get_franchise_with_company(self, name):
        return self.franchise.company != None

    @property
    def invoice_party(self):
        if self.party:
            return self.party
        if self.franchise.ticket_party:
            return self.franchise.ticket_party

    @property
    def invoice_key(self):
        'Returns the key used to group the invoices'
        date_format = "%Y%m%d"
        day = self.ticket_date.strftime(date_format)
        return (day, self.franchise.id, self.invoice_party)

    @property
    def manual_invoice_key(self):
        'Returns the key used to group the invoices (manual)'
        # With manual, we can invoice different dates
        return self.invoice_key[1:]

    def get_invoice(self, automatic):
        'Returns the invoice for the current ticket'
        pool = Pool()
        Invoice = pool.get('account.invoice')
        Journal = pool.get('account.journal')

        journals = Journal.search([
                ('type', '=', 'revenue'),
                ], limit=1)
        if journals:
            journal, = journals
        else:
            journal = None

        invoice = Invoice()
        invoice.journal = journal
        invoice.type = 'out'
        invoice.party = self.invoice_party
        invoice.description = "Emesa per: "+self.franchise.name
        invoice.payment_type = self.payment_type
        invoice.company = self.franchise.company
        if automatic:
            invoice.invoice_date = self.ticket_date.date()
        for key, value in invoice.on_change_party().iteritems():
            setattr(invoice, key, value)
        invoice.lines = []
        invoice.dlines = {}  # helper dict
        return invoice

    def _get_invoice_lines(self, sequence, lines, info):
        'Returns the invoice lines for the current ticket'
        header_sequence = sequence
        if not info['automatic']:
            sequence += 1

        has_lines = False
        for line in self.lines:
            if not line.to_invoice:
                continue

            has_lines = True
            if info['automatic']:
                key = line.invoice_line_key
            else:
                key = line.manual_invoice_line_key

            # dict.get executes EVERY TIME default function!
            if key in lines:
                invoice_line = lines[key]
            else:
                invoice_line = line.get_invoice_line_head(sequence, info)

            line.add_to_invoice_line(invoice_line, info)
            lines[key] = invoice_line
            sequence += 1
        if not has_lines:
            return

        if not info['automatic']:
            lines.setdefault(('header', self.idticket),
                self.get_invoice_header_line(header_sequence))
            lines.setdefault(('footer', self.idticket),
                self.get_invoice_footer_line(sequence))
            sequence += 1

    def get_invoice_header_line(self, sequence):
        'Return invoice header line'
        InvoiceLine = Pool().get('account.invoice.line')
        line = InvoiceLine()
        line.company = self.franchise.company
        line.invoice_type = 'out'
        line.type = 'title'
        line.description = "Tiquet "+str(self.numticket)
        line.sequence = sequence
        return line

    def get_invoice_footer_line(self, sequence):
        'Return invoice header line'
        InvoiceLine = Pool().get('account.invoice.line')
        line = InvoiceLine()
        line.company = self.franchise.company
        line.invoice_type = 'out'
        line.type = 'subtotal'
        line.description = "Subtotal "+str(self.numticket)
        line.sequence = sequence
        return line

    @classmethod
    def _invoice(cls, tickets, automatic=False):
        pool = Pool()
        Invoice = pool.get('account.invoice')
        Tax = pool.get('account.tax')
        Config = pool.get('syncbgb.config')

        # Logging
        logger = logging.getLogger('syncbgb')

        info = {}
        info['automatic'] = automatic

        logger.info('Starting ticket invoicing')
        if automatic:
            logger.info('- Automatic mode (group lines)')

        invoices = {}
        for ticket in tickets:
            if not ticket.franchise.company:
                continue
            if automatic and ticket.special_invoice:
                continue
            company_id = ticket.franchise.company.id

            with Transaction().set_context(company=company_id):
                # Prepare company taxes, and grouped plu
                config = Config(1)
                taxes = Tax.search(['OR', [
                        ('template', '=', config.tax_sre.id)
                    ], [
                        ('template', '=', config.tax_red.id)
                    ], [
                        ('template', '=', config.tax_gen.id)
                    ]
                ])
                info['tax_dict'] = {int(tax.rate*100): tax for tax in taxes}
                info['group_plu'] = config.group_plu

                # Re-browse with correct context
                ticket = cls(ticket)
                if not ticket.invoice_party:
                    cls.raise_user_error('missing_party', ticket.rec_name)
                if automatic:
                    key = ticket.invoice_key
                else:
                    key = ticket.manual_invoice_key

                # dict.get executes EVERY TIME default function!
                if key in invoices:
                    invoice = invoices[key]
                else:
                    invoice = ticket.get_invoice(automatic)

                try:
                    sequence = max(l.sequence
                        for l in invoice.dlines.values())+1
                except ValueError:
                    sequence = 1

                ticket._get_invoice_lines(sequence, invoice.dlines, info)
                invoices[key] = invoice

            # Update party on tickets
            if not ticket.party:
                ticket.party = ticket.franchise.ticket_party
                ticket.save()

        created = []
        to_create = defaultdict(list)
        for invoice in invoices.values():
            if not invoice.dlines:
                continue
            invoice.lines = invoice.dlines.values()  # Lines dict to list
            del invoice.dlines

            if hasattr(Invoice, 'aeat340_records'):  # aeat_340 installed
                invoice_tickets = {tl.ticket.id for il in invoice.lines
                    if il.type == 'line'
                    for tl in il.ticket_lines}
                if len(invoice_tickets) == 1:
                    for inv_line in invoice.lines:
                        inv_line.aeat340_operation_key = 'J'

            to_create[invoice.company.id].append(invoice)
        for company, values in to_create.iteritems():
            with Transaction().set_context(company=company):
                new = Invoice.create([i._save_values for i in values])
                Invoice.update_taxes(new)
                created.extend(new)
                if automatic:
                    # If automatic, post the invoices
                    Invoice.post(new)

        logger.info('%s tickets processed - %s invoices created'
            % (len(tickets), len(created)))
        logger.info('Finishing ticket invoicing')
        return created

    @classmethod
    @ModelView.button
    def invoice(cls, tickets):
        cls._invoice(tickets)

    @classmethod
    def import_cron(cls):
        pool = Pool()
        Wizard = pool.get('syncbgb.import_tickets', type='wizard')
        session_id, _, _ = Wizard.create()
        wizard = Wizard(session_id)
        wizard.importar()  # it raise an exeption if cant use DB
        return True

    @classmethod
    def invoice_cron(cls):
        pool = Pool()
        Wizard = pool.get('syncbgb.create_invoices', type='wizard')
        session_id, _, _ = Wizard.create()
        wizard = Wizard(session_id)
        wizard.invoice()  # it raise an exeption if cant use DB
        return True


class TicketLine(ModelSQL, ModelView):
    'TicketLine'
    __name__ = 'syncbgb.ticket.line'

    # Camps fecha i hora s'ajunten i van a create_date, que es timestamp
    ticket = fields.Many2One('syncbgb.ticket', 'Ticket', ondelete='CASCADE',
        select=True)

    sequence = fields.Integer('Sequence', required=True)
    line_date = fields.DateTime('Fecha', required=True)
    plu = fields.Integer('PLU', required=True)
    product = fields.Many2One('product.product', 'Product', required=True)
    quantity = fields.Integer('Quantity')
    weight = fields.Integer('Weight')
    percenttax = fields.Integer('% TAX')
    net = fields.Float('Net', digits=(16, Eval('currency_digits', 2)),
        required=True, depends=['currency_digits'])
    tax = fields.Float('Tax', digits=(16, Eval('currency_digits', 2)),
        required=True, depends=['currency_digits'])
    unitprice = fields.Float('Unit price',
        digits=(16, Eval('currency_digits', 2)), required=True,
        depends=['currency_digits'])
    amount = fields.Float('Amount', digits=(16, Eval('currency_digits', 2)),
        required=True, depends=['currency_digits'])
    discount = fields.Float('Discount',
        digits=(16, Eval('currency_digits', 2)), required=True,
        depends=['currency_digits'])
    total = fields.Float('Total', digits=(16, Eval('currency_digits', 2)),
        required=True, depends=['currency_digits'])
    uom = fields.Many2One('product.uom', 'Unit of Measure', required=True)
    currency_digits = fields.Function(fields.Integer('Unit Digits'),
        'on_change_with_currency_digits')
    invoice_line = fields.Many2One('account.invoice.line', 'Invoice Line',
        select=True)

    @classmethod
    def __setup__(cls):
        super(TicketLine, cls).__setup__()
        cls._error_messages.update({
            'unknown_tax': 'The tax "%s" is not in syncbgb configuration.'
        })

    @fields.depends('ticket')
    def on_change_with_currency_digits(self, name=None):
        if self.ticket:
            return self.ticket.currency_digits
        return 2

    @property
    def to_invoice(self):
        return self.invoice_line == None

    @property
    def invoice_line_key(self):
        'Returns the key used to group the invoice lines'
        return (self.percenttax, self.product.template.account_revenue_used)

    @property
    def manual_invoice_line_key(self):
        'Returns the key used to group the invoice lines (manual)'
        # With manual, we dont group lines
        return (self.id,)

    def get_invoice_line_head(self, sequence, info):
        pool = Pool()
        try:
            Aeat340Type = pool.get('aeat.340.type')
        except KeyError:
            Aeat340Type = None
        InvoiceLine = pool.get('account.invoice.line')
        Selection = pool.get('analytic_account.account.selection')

        line = InvoiceLine()
        # Basic information
        line.company = self.ticket.franchise.company
        line.party = self.ticket.invoice_party
        line.invoice_type = 'out'
        line.sequence = sequence

        if info['automatic']:
            line.product = info['group_plu']
            line.uom = info['group_plu'].default_uom
        else:
            line.product = self.product
            line.uom = self.product.template.default_uom
            line.origin = self

        for key, value in line.on_change_product().iteritems():
            setattr(line, key, value)

        if self.percenttax not in info['tax_dict']:
            self.raise_user_error('unknown_tax', (str(self.percenttax),))
        tax = info['tax_dict'][self.percenttax]
        line.taxes = [tax.id]
        line.quantity = 0
        line.unit_price = Decimal(0)

        if hasattr(InvoiceLine, 'aeat340_operation_key'):
            # aeat_340 installed
            book_keys = Aeat340Type.search([
                    ('book_key', '=', 'E'),
                    ])
            if book_keys:
                line.aeat340_book_key = book_keys[0]
                line.aeat340_operation_key = 'B'

        # Analytic account
        if self.ticket.franchise.analytic_account:
            selection = Selection()
            selection.accounts = [self.ticket.franchise.analytic_account]
            line.analytic_accounts = selection

        line.ticket_lines = []

        return line

    def add_to_invoice_line(self, line, info):
        pool = Pool()
        InvoiceLine = pool.get('account.invoice.line')
        digits = InvoiceLine.unit_price.digits[1]

        if info['automatic']:
            line.quantity = 1
            line.unit_price += Decimal(self.net).\
                quantize(Decimal(str(10 ** -digits)))
        else:
            # Unit price
            line.unit_price = Decimal(
                self.unitprice/(1+self.percenttax/100.0)).\
                quantize(Decimal(str(10 ** -digits)))

            # Quantity (can be weight (kg/g) or quantity (unit))
            if self.weight and \
                    line.product.template.default_uom.symbol == 'kg':
                line.quantity = round(float(self.weight / 1000.0), 3)
            elif self.weight:
                # Exception: Product with uom changed from kg to u
                line.quantity = 1
                line.unit_price = Decimal(self.net).\
                    quantize(Decimal(str(10 ** -digits)))
            else:
                line.quantity = float(self.quantity)

            # Discount
            if self.discount != 0:
                line.gross_unit_price = line.unit_price
                digits = InvoiceLine.discount.digits[1]
                line.discount = Decimal(self.ticket.percentdiscount/100.0)\
                    .quantize(Decimal(str(10 ** -digits)))
                prices = line.update_prices()
                line.gross_unit_price = prices['gross_unit_price']
                line.unit_price = prices['unit_price']
        line.ticket_lines.append(self)


class ImportTicketsStart(ModelView):
    'Import Tickets Start'
    __name__ = 'syncbgb.import_tickets.start'


class ImportTickets(Wizard):
    'Import Tickets'
    __name__ = 'syncbgb.import_tickets'

    start = StateView('syncbgb.import_tickets.start',
        'syncbgb.import_tickets_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Import', 'imp', 'tryton-ok', default=True),
            ])

    imp = StateAction('syncbgb.act_ticket_form')

    @classmethod
    def __setup__(cls):
        super(ImportTickets, cls).__setup__()
        cls._error_messages.update({
            'unknown_franchise': 'Unknown franchise: "%s".',
            'unknown_tax': 'Unknown tax: "%s".',
            'unknown_uom': 'Unknown uom: "%s".',
            'unknown_payment': 'Unknown payment type: "%s".'
        })

    @staticmethod
    def import_info():
        '''Returns a dictionary with the info of tables,
        fields and orders to import'''
        return {
            'head': {
                'table': 'Cabecera_tiquets',
                'fields':  ['fecha', 'idticket', 'tienda', 'balanza',
                    'vendedor', 'importe', 'sre', 'iva_sre', 'red', 'iva_red',
                    'gen', 'iva_gen', 'siniva', 'metodo_pago',
                    'porcentaje_descuento', 'descuento', 'numtiquet'],
                'order': ['fecha']
            },
            'lines': {
                'table': 'Linea_tiquets',
                'fields':  ['idticket', 'idlinea', 'fecha', 'plu', 'cantidad',
                    'peso', 'preciounitario', 'tipo_iva', 'porcentaje_iva',
                    'neto', 'iva', 'importe', 'descuento', 'total',
                    'unidadventa'],
                'order': ['idticket', 'idlinea']
            }
        }

    def ticketFromLine(self, line, config, franchise):
        ticket = {}

        # Ticket date comes in Europe/Madrid timezone,
        # and Tryton works with UTC dates
        ticket_date = line[0]
        ticketzone = pytz.timezone("Europe/Madrid")
        ticket_date_localized = ticketzone.localize(ticket_date,
            is_dst=None)
        ticket_date_utc = ticket_date_localized.\
            astimezone(pytz.utc)

        txtPaymentType = line[13]
        if txtPaymentType == 1:
            paymenttype = config.payment_cash
        elif txtPaymentType == 4:
            paymenttype = config.payment_card
        elif txtPaymentType == 5:
            paymenttype = config.payment_credit
        else:
            self.raise_user_error('unknown_payment',
                (txtPaymentType,))

        ticket['ticket_date'] = ticket_date_utc
        ticket['idticket'] = line[1]
        ticket['franchise'] = franchise
        ticket['scale'] = line[3]
        ticket['seller'] = line[4]
        # TODO: Can use Eval for get default round number?
        ticket['amount'] = round(line[5] or 0, 2)
        ticket['net_sre'] = round(line[6] or 0, 2)
        ticket['tax_sre'] = round(line[7] or 0, 2)
        ticket['net_red'] = round(line[8] or 0, 2)
        ticket['tax_red'] = round(line[9] or 0, 2)
        ticket['net_gen'] = round(line[10] or 0, 2)
        ticket['tax_gen'] = round(line[11] or 0, 2)
        ticket['net_notax'] = round(line[12] or 0, 2)
        ticket['payment_type'] = paymenttype
        ticket['percentdiscount'] = line[14]
        ticket['discount'] = line[15]
        ticket['numticket'] = line[16]

        return ticket

    def importTicketLines(self, cur, tickets, config, franchise):
        pool = Pool()
        Product = pool.get('product.product')

        info = self.import_info()
        linezone = pytz.timezone("Europe/Madrid")

        ticket_ids = []  # ID list to do one query to all
        # Map to get tryton object for every idticket (util for lines)
        ticket_map = {}
        for ticket in tickets:
            ticket_ids.append(ticket.idticket)
            ticket_map[ticket.idticket] = ticket

        cur.execute("SELECT "+",".join(info["lines"]["fields"])+" \
                    FROM \""+info["lines"]["table"]+"\" WHERE tienda = %s AND \
                    idticket IN %s ORDER BY "+",".join(info["lines"]["order"]),
                    (franchise.code, tuple(ticket_ids),))

        ticketlines = []
        invoicetickets = []
        for line in cur:
            ticketline = {}
            ticket = ticket_map[line[0]]

            txtUOM = line[14]
            if txtUOM == "Un":
                uom = config.uom_un.id
            elif txtUOM == "Kg":
                uom = config.uom_kg.id
            else:
                self.raise_user_error('unknown_uom', (txtUOM,))

            plu = line[3]
            products = Product.search([
                    ('template.plu', '=', str(plu))
                ])
            if len(products):
                p = products[0]
                if p == config.invoice_plu:
                    # False product for mark ticket to invoice
                    invoicetickets.append(ticket)
                    continue
                else:
                    product = p.id
            else:
                product = config.no_plu.id

            # Ticket date comes in Europe/Madrid timezone,
            # and Tryton works with UTC dates
            line_date = line[2]
            line_date_localized = linezone.localize(line_date,
                is_dst=None)
            line_date_utc = line_date_localized.astimezone(pytz.utc)

            ticketline['sequence'] = line[1]
            ticketline['line_date'] = line_date_utc
            ticketline['plu'] = plu
            ticketline['product'] = product
            ticketline['quantity'] = line[4]
            ticketline['weight'] = line[5]
            ticketline['unitprice'] = round(line[6] or 0, 2)
            ticketline['percenttax'] = line[8]
            ticketline['net'] = round(line[9] or 0, 2)
            ticketline['tax'] = round(line[10] or 0, 2)
            ticketline['amount'] = round(line[11] or 0, 2)
            ticketline['discount'] = round(line[12] or 0, 2)
            ticketline['total'] = round(line[13] or 0, 2)
            ticketline['uom'] = uom

            ticketline['ticket'] = ticket.id

            ticketlines.append(ticketline)

        TicketLine.create(ticketlines)
        return cur.rowcount, invoicetickets

    def markToInvoice(self, invoicetickets=None):
        pool = Pool()
        Ticket = pool.get('syncbgb.ticket')
        if invoicetickets:
            tickets = Ticket.search([
                ('id', 'in', [t.id for t in invoicetickets]),
                ])
            Ticket.write(tickets, {
                'special_invoice': True,
                })

    def importar(self):
        pool = Pool()
        Config = pool.get('syncbgb.config')
        Ticket = pool.get('syncbgb.ticket')
        Franchise = pool.get('sale.franchise')

        # Logging
        logger = logging.getLogger('syncbgb')

        logger.info('Starting syncronitzation of tickets')

        # Before start, assert the connection
        Wizard = pool.get('syncbgb.test_connection', type='wizard')
        session_id, _, _ = Wizard.create()
        wizard = Wizard(session_id)
        wizard.transition_test()  # it raise an exeption if cant use DB

        # Get config (singleton)
        config = Config(1)

        conn = psycopg2.connect(host=config.host, port=config.port,
            database=config.database, user=config.user,
            password=config.password)
        cur = conn.cursor()

        info = self.import_info()

        newticketids = []  # To return all new created ticket IDS

        logger.info('Successfully connected to DB')
        franchises = Franchise.search([])  # get all franchises
        for franchise in franchises:

            # Search the last ticket imported for this franchise
            last_tickets = Ticket.search([
                    ('franchise', '=', franchise.id)
                ], order=[('idticket', 'DESC')], limit=1)
            last_ticket_id = 0
            if last_tickets:
                last_ticket_id = last_tickets[0].idticket

            logger.info(("Executing query to get new tickers (idticket > %s) "
                "of franchise %s.") % (last_ticket_id, franchise.code))

            cur.execute("SELECT "+",".join(info["head"]["fields"])+" FROM \
                \""+info["head"]["table"]+"\" \
                WHERE tienda = %s AND idticket > %s \
                ORDER BY "+",".join(info["head"]["order"]),
                (franchise.code, last_ticket_id))

            numtickets = cur.rowcount

            if cur.rowcount:

                tickets = []
                for line in cur:
                    tickets.append(self.ticketFromLine(line, config,
                        franchise))

                tickets = Ticket.create(tickets)

                newticketids.extend([t.id for t in tickets])

                numlines, invoicetickets = self.importTicketLines(cur,
                    tickets, config, franchise)

                # If necessary, set tickets as special invoice
                self.markToInvoice(invoicetickets)

                logger.info(("Query successfully executed and tickets "
                    "imported. Got %s tickets, %s lines.")
                    % (numtickets, numlines))
            else:
                logger.info(("Query successfully executed. "
                    "No tickets to import."))

        cur.close()
        conn.close()

        logger.info('All imported. Finishing...')

        return newticketids

    def do_imp(self, action):

        newticketids = self.importar()

        data = {'res_id': newticketids}
        if len(newticketids) == 1:
            action['views'].reverse()
        return action, data


class ReimportTicketsStart(ModelView):
    'Reimport Tickets Start'
    __name__ = 'syncbgb.reimport_tickets.start'

    franchise = fields.Many2One('sale.franchise', 'Franchise', required=True)
    start_date = fields.DateTime('Start date', required=True,
        domain=[
            ('start_date', '<', Eval('end_date')),
            ], depends=['end_date'])
    end_date = fields.DateTime('End date', required=True,
        domain=[
            ('end_date', '>', Eval('start_date')),
            ], depends=['start_date'])


class ReimportTickets(Wizard):
    'Reimport Tickets'
    __name__ = 'syncbgb.reimport_tickets'

    start = StateView('syncbgb.reimport_tickets.start',
        'syncbgb.reimport_tickets_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Reimport', 'imp', 'tryton-ok', default=True),
            ])

    imp = StateAction('syncbgb.act_ticket_form')

    def do_imp(self, action):
        pool = Pool()
        Config = pool.get('syncbgb.config')
        Ticket = pool.get('syncbgb.ticket')
        # TicketLine = pool.get('syncbgb.ticket.line')

        franchise = self.start.franchise
        start_date = self.start.start_date
        end_date = self.start.end_date

        # Logging
        logger = logging.getLogger('syncbgb')
        logger.info('Starting ticket reimport')

        # Before start, assert the connection
        Wizard = pool.get('syncbgb.test_connection', type='wizard')
        session_id, _, _ = Wizard.create()
        wizard = Wizard(session_id)
        wizard.transition_test()  # it raise an exeption if cant use DB

        # Get config (singleton)
        config = Config(1)

        conn = psycopg2.connect(host=config.host, port=config.port,
            database=config.database, user=config.user,
            password=config.password)
        cur = conn.cursor()

        logger.info('Successfully connected to DB')

        info = ImportTickets.import_info()

        # Prepare ImportTickets wizard
        Wizard = pool.get('syncbgb.import_tickets', type='wizard')
        session_id, _, _ = Wizard.create()
        w_import = Wizard(session_id)

        # Convert start/end dates timezone
        local_tz = pytz.timezone("Europe/Madrid")
        start_date_localized = pytz.utc.localize(start_date)\
            .astimezone(local_tz)
        end_date_localized = pytz.utc.localize(end_date)\
            .astimezone(local_tz)

        # Get sinc tickets for this filter
        cur.execute("SELECT "+",".join(info["head"]["fields"])+" FROM \
                \""+info["head"]["table"]+"\" \
                WHERE tienda = %s AND fecha BETWEEN %s AND %s \
                ORDER BY "+",".join(info["head"]["order"]),
                (franchise.code, start_date_localized.isoformat(),
            end_date_localized.isoformat()))

        # Get tryton tickets for this filter
        actual_tickets = Ticket.search([
            ('franchise', '=', franchise.id),
            ('ticket_date', '>=', start_date),
            ('ticket_date', '<=', end_date)
        ])

        newticketids = []

        if cur.rowcount:

            tickets_to_create = []
            # tickets = []
            for line in cur:
                # Is ticket already imported?
                for actual_ticket in actual_tickets:
                    if actual_ticket.idticket == line[1]:
                        # tickets.append(actual_ticket)
                        break
                else:
                    # If dont have the ticket, import it!
                    tickets_to_create.append(w_import.ticketFromLine(line,
                        config, franchise))

            tickets_to_create = Ticket.create(tickets_to_create)
            newticketids = [t.id for t in tickets_to_create]

            # Now Just import new ticket lines (other can be invoiced)
            # tickets.extend(tickets_to_create)
            # newticketids = [t.id for t in tickets]
            # # Delete lines (will be reimported)
            # actual_lines = TicketLine.search([
            #     ('ticket', 'in', newticketids)
            # ])
            # TicketLine.delete(actual_lines)

            # Import ALL lines (old tickets and new imported tickets)
            numlines, invoicetickets = w_import.importTicketLines(cur,
                tickets_to_create, config, franchise)

            # If necessary, set tickets as special invoice
            w_import.markToInvoice(invoicetickets)

            logger.info(("Query successfully executed and tickets imported. "
                "Got %s tickets, %s lines.")
                % (len(newticketids), numlines))
        else:
            logger.info(("Query successfully executed. "
                "No tickets to import."))

        cur.close()
        conn.close()
        logger.info('All reimported. Finishing...')

        data = {'res_id': newticketids}
        if len(newticketids) == 1:
            action['views'].reverse()
        return action, data


class CreateInvoicesStart(ModelView):
    'Create Invoices Start'
    __name__ = 'syncbgb.create_invoices.start'

    date = fields.Date('Date', required=True)
    company = fields.Many2One('company.company', 'Company')

    @staticmethod
    def default_date():
        Date = Pool().get('ir.date')
        return Date.today()


class CreateInvoices(Wizard):
    'Create Invoices'
    __name__ = 'syncbgb.create_invoices'
    start = StateView('syncbgb.create_invoices.start',
        'syncbgb.create_invoices_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('OK', 'create_invoices', 'tryton-ok', True),
            ])
    create_invoices = StateAction(
            'account_invoice.act_invoice_out_invoice_form')

    def get_domain_to_invoice(self):
        pool = Pool()
        Invoice = pool.get('account.invoice')
        InvoiceLine = pool.get('account.invoice.line')
        TicketLine = pool.get('syncbgb.ticket.line')
        ticket_line = TicketLine.__table__()
        invoice_line = InvoiceLine.__table__()
        invoice = Invoice.__table__()
        domain = [
            ('franchise.company', '!=', None),
            ('special_invoice', '=', False),
            ('lines', 'in', ticket_line.
                join(invoice_line,
                    condition=invoice_line.id == ticket_line.invoice_line,
                    type_='left').
                join(invoice, condition=invoice.id == invoice_line.invoice,
                    type_='left').
                select(ticket_line.id,
                    where=((ticket_line.invoice_line == Null) |
                    (invoice.state == 'cancel')))),
            ]
        try:
            if self.start.date:
                domain.append([('ticket_date', '<=',
                    datetime.datetime.combine(self.start.date,
                    datetime.time.max))])
            if self.start.company:
                domain.append([('franchise.company', '=',
                    self.start.company.id)])
        except AttributeError:
            pass  # From cron, self.start dont exists

        return domain

    def invoice(self):
        pool = Pool()
        Ticket = pool.get('syncbgb.ticket')
        tickets = Ticket.search(self.get_domain_to_invoice(),
            order=[('ticket_date', 'ASC')])
        invoices = Ticket._invoice(tickets, True)
        return invoices

    def do_create_invoices(self, action):
        invoices = self.invoice()
        data = {'res_id': [i.id for i in invoices]}
        return action, data


class ReinvoiceStart(ModelView):
    'Reinvoice Start'
    __name__ = 'syncbgb.reinvoice.start'

    franchise = fields.Many2One('sale.franchise', 'Franchise', required=True,
        states={
            'readonly': Bool(Eval('old_party')),
        }, depends=['old_party'])
    old_party = fields.Many2One('party.party', 'Old Party', required=True,
        domain=[
            ('tickets.franchise', '=', Eval('franchise'))
        ], states={
            'readonly': (~Bool(Eval('franchise'))
                | Bool(Eval('tickets'))),
        }, depends=['franchise'])
    new_party = fields.Many2One('party.party', 'New Party', required=True)
    tickets = fields.One2Many('syncbgb.ticket', None, 'Tickets', required=True,
        add_remove=[
            ('franchise', '=', Eval('franchise')),
            ('party', '!=', None),
            ('party', '=', Eval('old_party')),
        ], states={
            'readonly': ~Bool(Eval('old_party')),
        }, depends=['franchise', 'old_party'])


class Reinvoice(Wizard):
    'Reinvoice'
    __name__ = 'syncbgb.reinvoice'
    start = StateView('syncbgb.reinvoice.start',
        'syncbgb.reinvoice_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('OK', 'reinvoice', 'tryton-ok', True),
            ])
    reinvoice = StateAction(
            'account_invoice.act_invoice_out_invoice_form')

    def do_reinvoice(self, action):
        pool = Pool()
        Ticket = pool.get('syncbgb.ticket')
        Invoice = pool.get('account.invoice')

        # Prepare ticket reinvoice (new party and empty line->invoice_line)
        franchise = self.start.franchise
        tickets = self.start.tickets
        Ticket.write(tickets, {
            'party': self.start.new_party.id,
            'special_invoice': True
        })

        # One of the invoices for the address (same client, so dont matter)
        old_invoice = tickets[0].invoices[0]

        # Reinvoice
        invoices = Ticket._invoice(tickets)

        # Create credit notes and change the party to old
        with Transaction().set_context(company=franchise.company.id):
            credit_invoices = Invoice.credit(invoices)
            Invoice.write(credit_invoices, {
                'party': self.start.old_party.id,
                'invoice_address': old_invoice.invoice_address.id,
                'send_address': old_invoice.send_address.id
            })

        data = {'res_id': [i.id for i in invoices]}
        return action, data
